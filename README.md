## BambooHR API interaction example

This is plain node.js code interacting with [BambooHR API](https://www.bamboohr.com/api/documentation).

Useful for an Amazon Lex bot we are creating.
