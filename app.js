'use strict';

const https = require('https');

// ---------------- API Constants --------------------------------------------------

const apiKey = '8a2f40e75354f3bfbeb2e98bc5f2d969da6f5b69';
const subDomain = 'winstontest';

const apiBaseOptions = {
    hostname: 'api.bamboohr.com',
    protocol: 'https:',
    path: '', // Set this depending on the endpoint
    method: 'GET',
    auth: apiKey+':x',
    headers: {
        'accept': 'application/json',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.8',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    }
};

const getAllEmployeesPath = '/api/gateway.php/'+subDomain+'/v1/employees/directory';

// ---------------- Helper Functions --------------------------------------------------

const capitalize = function(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

const findPerson = function(employees, firstName, lastName) {
    const findings = employees.filter(function (employee) {
        return employee.firstName === capitalize(firstName) && employee.lastName === capitalize(lastName)
    });
    if (findings.length > 0) {
        return findings[0];
    }
}

const filterVacations = function(timeOffItems) {
    let findings = timeOffItems.filter(function(timeOffItem) {
        return timeOffItem.name === 'Vacation'
    });
    if (findings.length > 0) {
        return findings[0];
    }
}

// ***************** BambooHR API **************

let employees = [];

const getEmployees = function (doSomething) {
    const callback = function(resp) {
        let data = ''

        resp.on('data', function (chunk) {
            data += chunk;
        });

        resp.on('end', function () {
            var result = JSON.parse(data);
            employees = result.employees;
            
            doSomething(employees);
        });

        resp.on('error', function(e) {
            console.log('Error: ' + e.message);
        });
    }

    if (employees.length == 0) {
        var requestOptions = Object.assign({}, apiBaseOptions);
        requestOptions.path = getAllEmployeesPath;

        let req = https.request(requestOptions, callback);
        req.end();
    }
}

const getDays = function (firstName, lastName) {
    getEmployees( function(employees) {

        const callback = function(resp) {
            let data = ''

            resp.on('data', function (chunk) {
                data += chunk;
            });

            resp.on('end', function () {
                var result = JSON.parse(data);
                
                var vacations = filterVacations(result);

                //console.log(vacations);

                var hours = parseFloat(vacations.balance);
                var days = hours / 24.0;

                console.log( 'You have ' + days + ' days.' );
            });

            resp.on('error', function(e) {
                console.log('Error: ' + e.message);
            });
        }

        if (employees.length > 0) {
            var person = findPerson(employees, firstName, lastName);

            var requestOptions = Object.assign({}, apiBaseOptions);
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            let getTimeOffPath = '/api/gateway.php/'+subDomain+'/v1/employees/'+person.id+'/time_off/calculator/?end='+yyyy+ '-'+mm+'-'+dd;
            requestOptions.path = getTimeOffPath;

            let req = https.request(requestOptions, callback);
            req.end();
        }
    });
}

getDays("greivin", "lopez");


